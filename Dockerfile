FROM node:8.2

# Create app directory
RUN			  mkdir -p /srv/app
WORKDIR		/srv/app

# Copy code
COPY  package.json /srv/app/
RUN   npm install
COPY . /srv/app

#Docker variables
ENV DOCKER_EMAIL = .

# Port
EXPOSE		3000

CMD ["npm", "start"]

