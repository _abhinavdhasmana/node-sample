const Hapi = require('hapi');
const Good = require('good');
const routes = require('./routes');
const catboxRedis = require('catbox-redis');

const server = new Hapi.Server({
  cache: [
    {
      name: 'redisCache',
      engine: catboxRedis,
    },
  ],
});
server.connection({
  port: 3000,
});

server.route(routes(server));

server.register({
  register: Good,
  options: {
    reporters: {
      console: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{
          response: '*',
          log: '*',
        }],
      }, {
        module: 'good-console',
      }, 'stdout'],
    },
  },
}, (err) => {
  if (err) {
    throw err;
  }
});

if (!module.parent) {
  server.start((err) => {
    if (err) {
      throw err;
    }
    server.log(`Server running at: ${server.info.uri}`);
  });
}
module.exports = server;
