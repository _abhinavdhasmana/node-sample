# Current Node version #
v8.1.4

# Web server #
[hapi.js](https://hapijs.com/)

# Tests
[lab](https://github.com/hapijs/lab)

This is the basic bioler plate code using hapi as webserver and lab for tests. Its using [airbnb eslint](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb) which is run before running test and before code push.

To-Do:
* Add code for sequelize and sequelize-cli
* Add code for connection to Redis/Sequelize
* Use glue and confidence
* Add example for JOI validation
* Add PM2 for prod and nodemon for dev.
