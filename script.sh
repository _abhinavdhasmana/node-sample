git init
touch .gitignore
echo "node_modules" > .gitignore
npm init
(
  export PKG=eslint-config-airbnb;
  npm info "$PKG@latest" peerDependencies --json | command sed 's/[\{\},]//g ; s/: /@/g' | xargs npm install --save-dev "$PKG@latest"
)
npm install --save hapi
npm install --save good
npm install --save good-console
npm install --save good-squeeze
npm install --save-dev lab