const Lab = require('lab');
const chai = require('chai');
const server = require('../../server');
const spies = require('chai-spies');

exports.lab = Lab.script();
const lab = exports.lab;
const expect = chai.expect;
chai.use(spies);

lab.experiment('Hello', () => {
  lab.test('status code', (done) => {
    const options = {
      method: 'GET',
      url: '/',
    };
    server.inject(options, (response) => {
      expect(response).to.have.property('statusCode', 200);
      done();
    });
  });

  lab.test('response data', (done) => {
    const options = {
      method: 'GET',
      url: '/',
    };
    server.inject(options, (response) => {
      expect(response.result).to.eqls({testKey: 'testData'});
      done();
    });
  });

  lab.test('server method can be used', (done) => {
    expect(server.methods.sayHello).to.be.a('function');
    done();
  });

  lab.test('server method return right response', (done) => {
    server.methods.sayHello((err, result) => {
      expect(result).to.eqls({testKey: 'testData'});
      done();
    });
  });

  // TODO: Add a test case for checking if a function is called from cache
  // TODO: Add a test case to make sure cache is invalidated
});
