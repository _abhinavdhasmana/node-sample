const hello = require('./hello');

module.exports = (server) => {
  const routes = [].concat(hello(server));
  return routes;
};
