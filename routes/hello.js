
module.exports = (server) => {
  const sayHello = (next) => {
    server.log('Not loading from cache');
    next(null, {testKey: 'testData'});
  };
  server.method('sayHello', sayHello, {
    cache: {
      cache: 'redisCache',
      expiresIn: 30 * 1000,
      generateTimeout: 1000,
    },
  });

  const hello = [
    {
      method: 'GET',
      path: '/',
      handler: (request, reply) => {
        server.methods.sayHello((err, result) => {
          if (err) {
            server.log('error', err);
            return reply(err);
          }
          return reply(result);
        });
      },
    },
  ];
  return hello;
};

